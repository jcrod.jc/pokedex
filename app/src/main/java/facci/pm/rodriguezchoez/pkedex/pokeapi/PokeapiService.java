package facci.pm.rodriguezchoez.pkedex.pokeapi;

import facci.pm.rodriguezchoez.pkedex.models.PkemonRespuesta;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PokeapiService {
    @GET("pokemon")
    Call<PkemonRespuesta> obtenerListaPokemon(@Query("limit") int limit,@Query("offset") int offset);
}
