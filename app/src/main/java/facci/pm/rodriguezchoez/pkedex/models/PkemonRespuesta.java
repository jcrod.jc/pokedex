package facci.pm.rodriguezchoez.pkedex.models;

import java.util.ArrayList;

public class PkemonRespuesta {

    ArrayList<Pokemon> results;

    public ArrayList<Pokemon> getResults() {
        return results;
    }

    public void setResults(ArrayList<Pokemon> results) {
        this.results = results;
    }
}
