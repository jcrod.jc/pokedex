package facci.pm.rodriguezchoez.pkedex;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

import facci.pm.rodriguezchoez.pkedex.models.PkemonRespuesta;
import facci.pm.rodriguezchoez.pkedex.models.Pokemon;
import facci.pm.rodriguezchoez.pkedex.pokeapi.PokeapiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "POKEDEX";
    private Retrofit retrofit;

    private RecyclerView recyclerView;
    private ListaPokemonAdapter listaPokemonAdapter;
    private int offset;
    private boolean aptoParaCargar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);

      listaPokemonAdapter = new ListaPokemonAdapter(this);
        recyclerView.setAdapter(listaPokemonAdapter);
        recyclerView.setHasFixedSize(true);

        final GridLayoutManager layoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if(dy >0){
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItem = layoutManager.findFirstVisibleItemPosition();

                    if(aptoParaCargar){
                        if ((visibleItemCount + pastVisibleItem) >= totalItemCount){

                            Log.i(TAG, " LLegamos al final");

                            aptoParaCargar = false;
                            offset +=20;
                            obtenerDatos(offset);

                        }

                    }
                }
            }
        });


        retrofit = new Retrofit.Builder()
                .baseUrl("https://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        aptoParaCargar = true;
        offset = 0;
        obtenerDatos(offset);
    }

    private void obtenerDatos(int offset) {
        PokeapiService service = retrofit.create(PokeapiService.class);
        Call<PkemonRespuesta> pkemonRespuestaCall = service.obtenerListaPokemon(20,offset);

        pkemonRespuestaCall.enqueue(new Callback<PkemonRespuesta>() {
            @Override
            public void onResponse(Call<PkemonRespuesta> call, Response<PkemonRespuesta> response) {
                aptoParaCargar = true;
                if(response.isSuccessful()){
                    PkemonRespuesta pkemonRespuesta = response.body();
                    ArrayList <Pokemon> listaPokemon = pkemonRespuesta.getResults();
                   listaPokemonAdapter.adicionarListaPokemon(listaPokemon);

                }else{
                    Log.e(TAG," onResponse: " + response.errorBody());

                }

            }

            @Override
            public void onFailure(Call<PkemonRespuesta> call, Throwable t) {
                aptoParaCargar = true;
                Log.e(TAG," onFailure: " + t.getMessage());
            }
        });


    }
}
